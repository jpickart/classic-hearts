**Hearts**

The classic card game of Hearts.

**Project Notes**

This version uses the HTML5 canvas with JavaScript.

**Rules**

* The object of the game is to score the fewest points.
* The game ends when a player reaches 100 points.
* The 2 of Clubs must be lead to start the game.
* No Heart can be lead until a Heart has been played.
* Suit must be followed if possible.
* Players may "shoot the moon" by winning all points in a round. If this happens the player receives 0 points, and all other players receive 26 points.
​