// Hearts
// Copyright (c) 2019 Jason Pickart aka badger2013
// License: MIT
// Contact: pickart.jason@gmail.com
// Source: https://bitbucket.org/jpickart/classic-hearts/src

Hearts.Strings = {
    heartsNotBroken: "You cannot play Hearts until Hearts have been broken.",
    followSuit: "You must follow suit.",
    mustPlay2C: "You must play the 2 of Clubs to start the game.",
    cardsToPass: "Select three cards to pass",
    has2C: "You have the 2 of Clubs. The game will start when this has been played.",
    endGameMessage: "The game is over!",
    noPointsStart: "You must play a card that is not worth points on the first turn."
};
