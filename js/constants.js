// Hearts
// Copyright (c) 2019 Jason Pickart aka badger2013
// License: MIT
// Contact: pickart.jason@gmail.com
// Source: https://bitbucket.org/jpickart/classic-hearts/src

var HeartsConstants = {
    humanPlayer: "H",
    ace: "A",
    king: "K",
    queen: "Q",
    jack: "J",
    aceLong: "Ace",
    kingLong: "King",
    queenLong: "Queen",
    jackLong: "Jack",
    heart: "H",
    diamond: "D",
    spade: "S",
    club: "C",
    heartsFriendly: "Hearts",
    diamondsFriendly: "Diamonds",
    spadesFriendly: "Spades",
    clubsFriendly: "Clubs",
    playMode: "play",
    passMode: "pass",
    top: "top",
    middleLeft: "middleLeft",
    middleRight: "middleRight",
    bottom: "bottom",
    markPass: "markPass",
    markValid: "markValid",
    passLeft: "left",
    passRight: "right",
    passStay: "stay",
    passAcross: "across"
};
